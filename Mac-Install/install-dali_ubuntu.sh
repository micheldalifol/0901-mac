#!/bin/sh

# First step: add your user (`useradd dali`) in visudo (`visudo`)
# 2nd step: switch user to yout user (`su - dali`)
# 3rd step: To execute: save and `chmod +x ./install-dali_ubuntu.sh` then `./install-dali_ubuntu.sh`


###############################################################################
# Customize the script by passing values to the named parameters below, e.g.
# ./bootstrap-ubuntu-devops.sh --kubectl 1.20.2
nodejs=${nodejs:latest}
kubectl=${kubectl:latest}
terraform=${terraform:latest}
terragrunt=${terragrunt:latest}
packer=${packer:latest}
vault=${vault:latest}
keygen=${keygen:-true}
docker=${docker:latest}
docker-compose=${docker-compose:latest}

while [ $# -gt 0 ]; do
  if [[ $1 == *"--"* ]]; then
    param="${1/--/}"
    declare $param="$2"
    # echo $1 $2 # Optional to see the parameter:value result
  fi
  shift
done

###############################################################################
echo "Upgrade all serices..."
sudo apt-get -y update && sudo apt-get upgrade -y



echo 'export PATH="/usr/local/sbin:$PATH"' >> ~/.bashrc
echo '\n\n#alias\nalias ll="ls -liasahG"\nalias cl="clear"\nalias rmds="rm .DS_Store"\nalias duh="du -h -c . | egrep [0-9,]+G"\n\nlias dok="docker"\n\nalias tf="terraform"\n\nalias k="kubectl"' >> ~/.bashrc
echo 'source <(kubectl completion bash)' >>~/.bashrc
echo 'complete -o default -F __start_kubectl k' >>~/.bashrc


echo "Installing general utilities..."
sudo apt-get -y install python3 python3-pip python3-virtualenv python3-setuptools git default-jdk unzip build-essential vim nano jq openvpn wget bash-completion tree nmap filezilla
echo "Upgrade all serices..."
sudo apt-get -y update && sudo apt-get upgrade -y
reboot

echo "Installing venv..."



echo "Installation VSCode ..."
sudo apt install software-properties-common apt-transport-https wget -y
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
sudo apt install -y code

echo "Installing Node.js..."
curl -fsSL https://deb.nodesource.com/setup_${nodejs}.x | sudo bash -
sudo apt-get install -y nodejs

# echo "Installing Java ${java}..."
# sudo apt-get install openjdk-${java}-jdk

echo "Installing Ansible..."
# sudo apt-get-add-repository -y ppa:ansible/ansible
# sudo apt-get -y update
sudo apt-get -y install ansible


echo "Installing docker..."
sudo apt update
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
apt-cache policy docker-ce
sudo apt install -y docker-ce
sudo usermod -aG docker ${USER}
su - ${USER}
groups
sudo usermod -aG docker username

echo "Installing docker-compose...."
sudo curl -L "https://github.com/docker/compose/releases/download/latest/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose


# echo "Installing AWS CLI v2..."
# curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
# unzip awscliv2.zip
# sudo ./aws/install
# rm -f awscliv2.zip
# rm -rf ./aws

# echo "Installing kubectl ${kubectl}..."
# sudo apt-get -y update
# sudo apt-get -y install apt-transport-https gnupg2
# curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
# if grep -Fxq "deb https://apt.kubernetes.io/ kubernetes-xenial main" /etc/apt/sources.list.d/kubernetes.list
# then
#   echo "Not adding kubectl repo because it is already present"
# else
#   echo "Adding kubectl repo..."
#   echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
# fi
# sudo apt-get update -y
# sudo apt-get install -y kubectl=${kubectl}*

# echo "Installing the latest Helm..."
# curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
# chmod 700 get_helm.sh
# ./get_helm.sh
# rm ./get_helm.sh

echo "Installing Terraform ${terraform}..."
curl -o terraform.zip https://releases.hashicorp.com/terraform/${terraform}/terraform_${terraform}_linux_amd64.zip
unzip terraform.zip
sudo mv terraform /usr/local/bin/terraform
rm terraform.zip

# echo "Installing Terragrunt..."
# curl -o terragrunt -L https://github.com/gruntwork-io/terragrunt/releases/download/v${terragrunt}/terragrunt_linux_amd64
# sudo chmod +x terragrunt
# sudo mv terragrunt /usr/local/bin/terragrunt

# echo "Installing Packer ${packer}..."
# curl -o packer.zip https://releases.hashicorp.com/packer/${packer}/packer_${packer}_linux_amd64.zip
# unzip packer.zip
# sudo mv packer /usr/local/bin/packer
# rm packer.zip

# echo "Installing Vault ${vault}..."
# curl -o vault.zip https://releases.hashicorp.com/vault/${vault}/vault_${vault}_linux_amd64.zip
# unzip vault.zip
# sudo mv vault /usr/local/bin/vault
# rm vault.zip

echo "Installing labs environments.... VirtualBox..."
sudo apt-get install virtualbox

echo "Installing Postman..."
sudo apt install -y postman

echo "Cleaning up after bootstrapping..."
sudo apt-get -y update
sudo apt-get -y autoremove
sudo apt-get -y clean

# if [ $keygen = true ] ; then
#   echo "Checking if ssh key already exists..."
#   if [ ! -f ~/.ssh/id_rsa ] ; then
#     echo "Generating a new SSH key..."
#     ssh-keygen -q -t rsa -b 4096 -N "" -C "$(whoami)@$(hostname) on $(cat /etc/os-release)" -f ~/.ssh/id_rsa <<<y 2>&1 >/dev/null
#   else
#     echo "Key already exists"
#   fi
# fi



# Media tools
echo "Installing VLC Media Player"
sudo apt install -y vlc

echo "Installing ffmpeg..."
sudo apt install -y ffmpeg

# Browser
echo "Installing browsers..."
## installation de tor
sudo apt install -y tor torbrowser-launcher
## installation de brave
sudo apt install apt-transport-https curl
sudo curl -fsSLo /usr/share/keyrings/brave-browser-beta-archive-keyring.gpg https://brave-browser-apt-beta.s3.brave.com/brave-browser-beta-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-beta-archive-keyring.gpg arch=amd64] https://brave-browser-apt-beta.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-beta.list
sudo apt update
sudo apt install brave-browser-beta

#########################################################################
echo "Upgrade all serices..."
sudo apt-get -y update && sudo apt-get upgrade -y
sudo apt autoclean && sudo apt autoremove
#########################################################################

echo "Execute configuration ./configDali.sh"
./configDali.sh

echo "Lancer le script d'extension VSCode ./config_vscode.sh"
./config_vscode.sh
echo "penser dans les setting a mettre la tabulation a 2"


#installer composer
#echo "Installing composer..."
#curl -sS https://getcomposer.org/installer | php
#mv composer.phar /usr/local/bin/composer
#chmod +x /usr/local/bin/composer
#sudo composer self-update

echo "======================================================
Installations terminées !
======================================================"

