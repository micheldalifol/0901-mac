#!/bin/sh

## MD le 15 aout 2022
## Script d'installation d'extensions vscode


# connaitre les extensions installees
# code --list-extensions 
# [doc vscode command lines](https://code.visualstudio.com/docs/editor/command-line#_working-with-extensions)
# [doc vscode devcontainer cli](https://code.visualstudio.com/docs/remote/devcontainer-cli)

echo "Install extensions form vscode..."

code --install-extension ahebrank.yaml2json \
                        amazonwebservices.aws-toolkit-vscode \
                        bierner.markdown-yaml-preamble \
                        eightHundreds.vscode-drawio \
                        esbenp.prettier-vscode \
                        fnando.linter \
                        GitLab.gitlab-workflow \
                        hashicorp.terraform\
                        hediet.vscode-drawio \
                        mechatroner.rainbow-csv \
                        ms-azuretools.vscode-docker\
                        ms-kubernetes-tools.vscode-kubernetes-tools\
                        ms-python.python\
                        ms-python.vscode-pylance\
                        ms-python.isort \
                        ms-toolsai.jupyter\
                        ms-toolsai.jupyter-keymap\
                        ms-toolsai.jupyter-renderers\
                        ms-vscode-remote.remote-containers\
                        ms-vscode-remote.remote-ssh\
                        ms-vscode-remote.remote-ssh-edit\
                        ms-vscode.remote-explorer \
                        naumovs.color-highlight \
                        redhat.vscode-yaml\
                        TheHolyCoder.swagger-tools \
                        vscode-icons-team.vscode-icons \
                        wayou.vscode-todo-highlight \
                        wholroyd.jinja \
                        yzhang.markdown-all-in-one\
                        ZainChen.json\

echo "Installation over, everything OK !!!"
