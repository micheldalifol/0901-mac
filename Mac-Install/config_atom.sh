#!/bin/sh

# MD le 31 octobre 2021
# liste des themes et parametres à installer pour la configuration de Atom

## INSTALLATION

echo "1. Installer les Themes..."
apm install seti-ui
# https://atom.io/themes/seti-ui

apm install R2D2-syntax
# https://atom.io/themes/R2D2-syntax

echo "2. Installer les Packages..."
apm install atom-ide-ui
# https://atom.io/packages/atom-ide-ui
apm install platformio-ide-terminal
# https://atom.io/packages/platformio-ide-terminal
apm install atom-typescript
# https://atom.io/packages/atom-typescript
#apm isntall ide-php
# https://atom.io/packages/ide-php
apm install ide-json
# https://atom.io/packages/ide-json
#apm install pretty-json
# https://atom.io/packages/pretty-json
apm install ide-yaml
# https://atom.io/packages/ide-yaml
apm install ide-python
# https://atom.io/packages/ide-python
apm install language-javascript
#
apm install emmet
#
apm install atom-beautify
#
apm install highlight-selected
#
apm install file-icons
#
apm install pigments
#
apm install minimap
#
apm install color-picker
#
apm install simple-drag-drop-text
#
apm install autocomplete-paths
#
#apm install linter
#
apm install autoupdate-packages
#
apm install git-plus
#
#apm install api-workbench
#
apm install ftp-remote-edit
#
echo "Affiche la iste de packages obsoletes..."
apm outdated
#


## DESINSTALLATION
#echo "Clenup..."
#rm -r  ~/.atom /usr/local/bin/atom /usr/local/bin/apm /Applications/Atom.app ~/Library/Preferences/com.github.atom.* ~/Library/Application Support/com.github.atom.ShipIt ~/Library/Application Support/Atom ~/Library/Saved Application State/com.github.atom.savedState ~/Library/Caches/com.github.atom ~/Library/Caches/Atom
