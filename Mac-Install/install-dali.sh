#!/bin/sh

# Homebrew Script for OSX
# To execute: save and `chmod +x ./install-dali.sh` then `./install-dali.sh`


echo "Installing brew..."
#/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"


#mettre à jour le fichier ~/.zshrc
echo 'export PATH="/usr/local/sbin:$PATH"' >> ~/.zshrc
echo '\n\n#alias\nalias ll="ls -liasahG"\nalias cl="clear"\nalias rmds="rm .DS_Store"\nalias rmdsall="sudo find / -name ".DS_Store" -depth -exec rm {} \;"\nalias duh="du -h -c . | egrep [0-9,]+G"\n\nlias dok="docker"\n\nalias tf="terraform"\n\nalias k="kubectl"' >> ~/.zshrc
echo 'complete -F __start_kubectl k'
source <(kubectl completion zsh)  #active l'auto-complétion pour zsh dans le shell courant
echo "[[ $commands[kubectl] ]] && source <(kubectl completion zsh)" >> ~/.zshrc # ajoute l'auto-complétion de manière permanente à votre shell zsh

#Installation nécessaire depuis catalina
echo 'Installation Xcode Developpers Tools....'
xcode-select --install
brew install gcc


echo "Installing services list..."
brew services list

echo "Installing wget..."
brew install wget

echo "Installing brew completion..."
brew install bash-completion


#echo "Installing node et yarn ..."
#Programming Languages
#brew install yarn
#brew install node
    # node est installé avec yarn



echo "Installing DevOps Tools ..."
#Dev Tools
# brew install atom
brew install --cask visual-studio-code
#brew install sequel-pro
#brew install pyenv  #pour pouvoir avoir plusieurs versions de python qui cohabitent
#brew install git
#brew install gitlab-ci-local
#brew install gitlab-runner
#brew install diff-so-fancy     # permet de voir "surligné" les différences entre 2 fichiers
#brew install cassandra
brew install python
#brew install postman
#brew install ansible
brew install docker
brew install docker-compose
brew install docker-machine   # permet de créer des VM avec docker installé
#brew install terraform
#brew install kubectl
#brew install helm		# helm est un outil pour simplifier l'utilisation de Kubernetes
brew install tree

echo "Installing Web Tools ..."
#Web Tools
brew install tor-browser
#brew install google-chrome
#brew install firefox
#brew install opera
brew install onyx
#brew install teamviewer
#brew install anydesk
brew install the-unarchiver
#brew install vyprvpn


echo "Installing Network Tools..."
brew install nmap
brew install cyberduck


#echo "Installing Office Tools..."
#brew install adobe-acrobat-reader
#brew install gimp


echo "Installing Media Tools ..."
#Media Tools
brew install vlc
#brew install homebrew/cask/handbrake
#brew install makemkv
echo "installation ffmpeg avec toutes les options"
brew install ffmpeg $(brew options ffmpeg | grep -vE '\s' | grep -- '--with-' | tr '\n' ' ')
#brew install --cask 4k-video-downloader

#echo "Installing Slack..."
#brew install --cask slack

#echo "Installing Yac..."
# permet de lire les BD (.cbz, .cbr, ...)
#brew install yacreader

#echo "Installing retroGaming...."
#brew install mednafen  # emulateur de vieille console de jeux (https://mednafen.github.io/)

#permet d'installer locate et updatedb
#echo "Installing locate and updatedb..."
#sudo launchctl load -w /System/Library/LaunchDaemons/com.apple.locate.plist
#sudo /usr/libexec/locate.updatedb
#sudo ln -s /usr/libexec/locate.updatedb /usr/local/bin/updatedb
#sudo updatedb
#echo 'Pour faire locate utiliser la syntaxe "locate -i <recherche>"'

#permet de démarrer le service de base de données
#brew services start mariadb
# => user: root
# => pass:
# => nom base données: à créer
# => lorsqu'on lance sequel pro on y accède

echo "Update, Upgrade and cleaning formulae..."
# tous les paquets brew
brew update && brew upgrade
brew cleanup


echo "Display outdated list packages...."
# affiche tous les paquets obsolètes
brew outdated

echo "Display brew's health on this mac..."
# affiche l'état de santé de vrew sur ce mac
echo "brew doctor..."
brew doctor
#echo "Corrige si besoin en suivant les instructions indiquées..."


#mettre en place une clé ssh
#cd ~`
#ls -lisahG		 => on vérifie si on a 1 dossier .ssh
#                         -> si non => on le crée     `mkdir .ssh`
#cd .ssh
#ssh-keygen -t rsa -C "email_personne_proprio_du_mac"
#        entree
#        entree

#echo "dans le dossier .ssh se trouve des fichiers:"
#echo " id_rsa: clé privée à garder sécurisée"
#echo "  id_rsa_pub: clé publique à transmettre"


#echo "Ajout dans la crontab de la suppression des fichier .DS_Store"
#sudo crontab -e
#00 12 * * mon root find / -name ".DS_Store" -depth -exec rm {} \;

# echo "Lancer le script de configuation d'atom ./config_atom.sh"
# ./config_atom.sh

echo "Lancer le script d'extension VSCode ./config_vscode.sh"
./config_vscode.sh
echo "penser dans les setting a mettre la tabulation a 2"

#installer composer
#echo "Installing composer..."
#curl -sS https://getcomposer.org/installer | php
#mv composer.phar /usr/local/bin/composer
#chmod +x /usr/local/bin/composer
#sudo composer self-update

echo "======================================================
Installations homebrew et IDE terminées !
======================================================"

