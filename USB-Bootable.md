# CREER UNE CLE USB BOOTABLE

1. Mettre la clé usb dans le lecteur usb   ATTENTION: ça doit être fait sur une clé usb           

>_Remarques_:            
> * Pour créer une **clé bootable macOS**, il faut formater au format **MacOs Etendu journalisé**              
> * Pour créer une **clé bootable rapsberry, linux**, il faut formater au format **MS-DOS (FAT et pas FAT32)**              

        
```bash
# 1. Download the ISO you want to install         

# 2. Convert the ISO         
`hdiutil convert [/path/to/downloaded.iso] -format UDRW -o [/path/to/newimage.img]`

# 3. Wite the image to usb        
  ## 3.1. get the list of disks (to get data of the usb key)
diskutil list

  ## 3.2. unmount the usb key
diskutil unmountDisk /dev/[diskX]

  ## 3.3. write the image to usb key
sudo dd if=[/path/to/newimage.dmg] of=/dev/[diskN] bs=1m
     
# 4. Boot on the usb key  
  ## 4.1. shutdown the computer
  ## 4.2. plug the usb key   
  ## 4.3. start the computer   
  ## 4.4. Click on Alt (or Cmr+R) while booting 
  ## 4.5. Choose usb key
  ## 4.6. Choose "Try" in the first time to be sure everything works as expected  
  ## 4.7. If everything is good, redo the actions 4.1 to 4.5 and choose "Install"
```    






## FORMATER UN DISQUE EN DIFFERENTS FORMATS

### FORMATER EN FAT32

`diskutil eraseDisk FAT32 <nomClé> /dev/disk2`

```bash
# <nomClé>: avec diskutil list on obtient:
dali:~ mdali$ diskutil list
  /dev/disk0 (internal, physical):
                             TYPE NAME                    SIZE       IDENTIFIER
     0:      GUID_partition_scheme                         251.0 GB   disk0
     1:                        EFI EFI                     209.7 MB   disk0s1
     2:                 Apple_APFS Container disk1         250.8 GB   disk0s2

  /dev/disk1 (synthesized):
                              TYPE NAME                    SIZE       IDENTIFIER
     0:      APFS Container Scheme -                       250.8 GB   disk1
                                   Physical Store disk0s2
     1:                APFS Volume Macintosh HD            77.5 GB    disk1s1
     2:                APFS Volume Preboot                 90.7 MB    disk1s2
     3:                APFS Volume Recovery                1.5 GB     disk1s3
     4:                APFS Volume VM                      1.1 GB     disk1s4

  /dev/disk2 (internal, physical):
                              TYPE NAME                    SIZE       IDENTIFIER
     0:      GUID_partition_scheme                         31.2 GB    disk2
     1:                        EFI EFI                     209.7 MB   disk2s1
     2:       Microsoft Basic Data NOOBS                   31.0 GB    disk2s2__
#  => ici nomclé serait par exemple "NOOBS" dans /dev/disk2
```

### FORMATER EN EXFAT

`diskutil eraseDisk EXFAT <nomClé> /dev/disk2`

```bash
dali:~ mdali$ diskutil eraseDisk exFAT NOOBS /dev/disk2
        Started erase on disk2
        Unmounting disk
        Creating the partition map
        Waiting for partitions to activate
        Formatting disk2s2 as ExFAT with name NOOBS
        Volume name      : NOOBS
        Partition offset : 411648 sectors (210763776 bytes)
        Volume size      : 60461056 sectors (30956060672 bytes)
        Bytes per sector : 512
        Bytes per cluster: 32768
        FAT offset       : 2048 sectors (1048576 bytes)
        # FAT sectors    : 8192
        Number of FATs   : 1
        Cluster offset   : 10240 sectors (5242880 bytes)
        # Clusters       : 944544
        Volume Serial #  : 5d692fe9
        Bitmap start     : 2
        Bitmap file size : 118068
        Upcase start     : 6
        Upcase file size : 5836
        Root start       : 7
        Mounting disk
        Finished erase on disk2
```

### FORMATER EN FAT (MS-DOS)

`diskutil eraseDisk MS-DOS <nomClé> /dev/disk2`

### FORMATER EN Journal Etendu MacOSX

`diskutil eraseDisk JHFS+ <nomClé> /dev/disk2`

### FORMATER EN Etendu MacOSX

`diskutil eraseDisk HFS+ <nomClé> /dev/disk2`

