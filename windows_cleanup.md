# COMMANDES WINDOWS CMD POUR NETTOYAGE PC

```bash
## nettoyage des lecteurs de votre ordinateur pour gagner de l'espace disque
cleanmgr

## analyser l'intégrité des fichiers et réparer si endommagés
sfc /scannow

##analyser le disque dur et de le réparer
chkdsk c: /f

## supprimer les fichiers temporaires
del /q/f/s %TEMP%\*

## nettoyage des fichiers de prélecture
%SystemRoot%\explorer.exe C:\Windows\prefetch\

## nettoyage de cache mémoire
ipconfig/flushDNS
wsreset.exe

## defragmenter un disque
defrag <lettre du lecteur>:

## supprimer les virus
### entrfer la lettre du lecteur (exemple lecteur F)
F
### chercher la liste des fichiers "virus" (ex: autorun.inf)
attrib -s -h -r /s /d *.*
### supprimer les fichiers virus
del <nomFichierVirus>   ## del autorun.inf
```