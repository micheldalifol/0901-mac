# NETTOYAGE MAC

## ONYX

installer   ->  Nettoyage       Tous les onglets
                Information     Memoire -> Purger


## TERMINAL

* lister tous les repertoires et leurs sous-repertoires
    `ls -lisahG`
    `ls -rtlh`    liste par ordre chronologique

* lister la taille des repertoires et de leurs sous-repertoires
```bash
du -h   # => si sur / -> liste tous les repertoires du machine
du -sh /path_repertoire  #pour 1 repertoire precis
                         #s de -sh indique à la commande de ne sortir qu'1 SEUL resultat global
```
* sur /   
=> .Spotlight-V100 : `sudo mdutil -Ea`  -> efface l'index et relance l'indexation
                                        => diminue la taille de .Spotlight-V100 et accélère le mac
>_REMARQUE_: si plusieurs disques =>indiquer le chemin du disques
            si l'index ne se refait pas automatiquement
                `sudo mdutil -ai on`

*  purger la RAM
    `sudo purge`

* fichier SleepImage (en fonction de sa taille). C'est le fichier où les données en RAM sont sauvegardées lorsque l'ordi se met en veille
    `du -h /private/var/vm/sleepimage`                => donne la taille du fichier
        Ce fichier peut très vite être très gros    => il faut limiter sa taille

1. Suppression du fichier       `sudo rm /private/var/vm/sleepimage`
2. Créer 1 nouveau vide         `sudo touch /private/var/vm/sleepimage`
3. Limiter sa taille            `sudo chflags uchg /private/var/vm/sleepimage`

* Supprimer les sauvegardes les plus anciennes liées aux Iphone:
    Library/ApplicationSupport/MobileSync/backup  ->

* Vérifier qu'un application a été totalement supprimée
    - pas de traces dans `/Library/`
    - pas de traces dans `/Library/ApplicationSupport/`


* Vérifier que le SIP est actif(Cette nouvelle fonctionnalité de sécurité protège votre Mac contre les logiciels dangereux et notamment le compte utilisateur root)
`csrutil status`        -> doit afficher: System Integrity Protection status: enabled.

* Supprimer les fichiers temporaires:`sudo periodic daily weekly monthly`

## PREFERENCES SYSTEME
compte      => ouverture    -> supprimer les applications non nécessaires à l'ouverture

## SCRIPT DE MAINTENANCE DE VOTRE MAC

Lancer la commande: `sudo periodic daily weekly monthly`
> répondre aux questions demandées: Permet de supprimer les fichiers temporaires.

## RÉINITIALISEZ LA NVRAM

Si votre Mac se comporte bizarrement, affiche une mauvaise résolution au démarrage, n’a plus de son ou prend beaucoup de temps à démarrer, il se peut que la mémoire NVRAM (mémoire RAM non volatile) soit corrompue. Bien que cela soit assez rare, voici la technique qui vous permettra de la réinitialiser :

1. Montez le volume au maximum puis éteignez le Mac
2. Retirez tous les périphériques USB à l’exception de votre clavier
3. Allumez votre Mac et dès que vous entendez le son de démarrage (Dong !), maintenez cette combinaison de touche `[Cmd + Option(alt) + P + R]` jusqu’à ce que vous entendiez la tonalité une deuxième fois.
4. Relâchez les touches et laissez votre ordinateur démarrer normalement.

## SUPPRIMEZ LES FICHIERS .DS_Store

> De manière manuelle
  `sudo find / -name ".DS_Store" -depth -exec rm {} \;`

> De manière automatique: crontab
  ```bash
sudo crontab -e
#<dans Vi modifier cette table>
 # suppression des fichiers ".DS_Store" toutes les heures
0 * * * * root find / -name ".DS_Store" -depth -exec rm {} \;
#<Minute> <Heure> <JourDuMois> <Mois> <JourDeLaSemaine> <Commande utilisateur>
```
Ici lance cette purge des fichiers .DS_Store tous les lundi à midi

## DESINSTALLER UNE APPLICATION

Pour désinstaller un logiciel, vous devez passer en revue chacun de ces dossiers et supprimer les éléments suivants :

1. Les fichiers binaires et les icônes du Dock se trouvent dans `/Applications/`
2. Les fichiers d'assistance des applications se trouvent dans `~/Library/Application`
3. Les caches d'assistance se trouvent dans `/Library/Caches/` et `~/Library/Caches`
4. Les modules se trouvent dans `~/Library/Address Book Plug-Ins/`
5. La bibliothèque se trouve dans `~/Library/`
6. Les préférences d'app se trouvent dans `~/Library/Preferences/`
7. Les rapports de plantage se trouvent dans `~/Library/Application Support/CrashReporter/`
8. Les états enregistrés des apps se trouvent dans `~/Library/Saved Application State/`
