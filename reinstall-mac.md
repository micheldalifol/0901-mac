1. réinstaller son mac:
	- `Cmd + R`:
		1. effacer le disque
		2. installer l'OS

	- Alt + Cmd + R:
		=> installe directement la dernière version compatible pour votre mac
			1. efface le disque
			2. installe l'OS

2. faire les mises à jour

3. coller les scripts "./install-dali.sh" et "./config_atom.sh" dans le même répertoire et s'assurer qu'ils soient exécutable avec 1 compte sudo

4. lancer le script d'intallation "./install-dali.sh"

5. Mettre le nom du hostname
`sudo scutil --set HostName <nouveauHostName-sansEspace>``
