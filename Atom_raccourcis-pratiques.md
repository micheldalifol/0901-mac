# RACCOURCIS PRATIQUES IDE

`Cmd + D`                                     selectionne tous les "morceaux" selectionnés
`Cmd + Ctrl + G `                             selectionne tous les même "morceaux"
`Cmd + /`                                     met en commentaire tout ce qui est selectionné
`Cmd + L`                                     selectionne ligne par ligne
`Cmd + Shift + J`                             selectionne tous les enfants
`Cmd + Shift + D`                             copie la ligne selectionnee
`Cmd + Crtl + Fleche haut(ou Fleche bas)`     change la position de la ligne selectonnee
`Cmd + Shift + curseur`                       pour mettre le curseur partout où on veut rajouter la même chose
`Cmd + Shift + /`                             block de commentaires
`Cmd + KU`                                    met en majuscule ce qui est selectionne
`Cmd + KL`                                    met en minuscule ce qui est selectionne
