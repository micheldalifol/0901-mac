# HOMEBREW

## DEFINITION

Gestionaire de paquets pour MacOSX.
[lien homebrew](https://brew.sh/index_fr)

## INSTALLATION

```bash
xcode-select --install # -> cliquez sur Installer
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

## MISE A JOUR

| Syntax | Definition |
| :--- | ---:|
| `brew update && brew upgrade`| **update et upgrade** de tous les paquets brew|
| `brew cask update && brew upgrade`| **update et upgrade** de tous les paquets cask|
| `brew outdated`  | _affiche_ une liste de tous les paquets obsolètes|
| `brew cleanup -n`  | _affiche_ les paquets obsolètes si une nouvelle version est installée|
| `brew cleanup <formula>`  | _nettoie_ les versions obsolètes du paquet|
| `brew pin <formula>`| empêche la mise à jour de certaines recettes en les épinglant|
| `brew unpin <formula>` |  **supprime** le blocage introduit par brew pin|
| `brew cask upgrade` |   **upgrade** tous les packages installés via cask_|


## RECETTES ET PAQUETS

* **formula**:	recettes
 _Fichier décrivant les étapes et les dépendances pour l'installation d'un paquet_
* **cellar**:	cellier _Dossier regroupant tous les paquets installés. En général_ **/usr/local/Cellar/**
* **bottles**:	bouteilles  _Versions précompilées d'un paquet_
* **taps**:	bondes  _Dépôts de recettes supplémentaires (dépot Github identifiés par un nom sous la forme User/Tap)_


## UTILISATION

* `brew doctor`  vérifier que brew est bien installé et qu'il est fonctionnel
* `brew cask doctor`  affiche des informations sur brew
* `brew install <nomPaquet>`  installer le paquet
* `brew remove <nomPaquet>`   désinstalle le paquet
* `brew search <nomRecette>`  recherche une application

##### 2 types de recettes existent:
- **formula**:   directement disponible pour être installée
- **formula alternative** (il y a un "/"): installable depuis une source alternative

* `brew home <nomRecette>`                      conduit à la page principale du logiciel
* `brew options <nomRecette>`                   lister les options d'une recette
* `brew commands`                               affiche les commandes de brew
* `man brew`
* `brew list`                                   affiche la liste des packages homebrew installés
* `brew cask list`                              affiche la liste des packages caskroom installés
* `brew deps --tree --installed`                affiche la liste des packages home brew installés avec leurs dépendances
* `brew list | grep <package-name>`             affiche de informations sur le package homebrew
* `brew cask list | grep <package-name>`        affiche de informations sur le package caskroom


##### Lors de la désinstallation d'un paquet, faire attention aux dépendances:

* `brew leaves`   affiche une liste des recettes qui peuvent être supprimé sans problème
* `brew missing`  affiche une liste des recettes manquantes, c'est-à-dire les recettes qui sont une dépendance d'un autre paquet installé, mais qui ne sont pas installés
* `brew uses foo` affiche une liste des recettes qui dépendent de foo
* `brew deps bar` affiche la liste des dépendances de bar
* `join <(brew uses <foo>) <(brew list)` affiche la liste des paquets installés qui dépendent de foo
* `join <(brew uses <foo>) <(brew list)` trouve les paquets installés qui dépendent d'un paquet foo

##### Supprimer un paquet et toutes ses dépendances:

`brew remove $(join <(brew leaves) <(brew deps foo))`  supprime foo puis trouve intersection entre les paquets qui ne sont pas des dépendances, et des dépendances de foo

##### Installer à partir d'un tap:

```bash
brew tap <user>/<tap>   # Clone le dépôt <user>/homebrew-<tap>
brew untap <user>/<tap> # Supprime la copie locale du dépôt
brew install <ma_formule_qui_provient_d_un_tap>
```

## A SAVOIR

- Brew installe les logiciels dans **/usr/local/bin** . Il n'est pas nécessaire de changer son PATH.

- Brew installe les logiciels dans son cellier, et créé un lien symbolique dans `/usr/local/bin` pour chacun des binaires installés, dans `/usr/local/include` pour chaque fichier d'en-tête, et dans `/usr/local/lib` pour chaque bibliothèque. Cela permet d'activer/désactiver rapidement et facilement un paquet en cas d'incompatibilité lors de l'installation ou de la compilation d'un autre logiciel.

- Brew permet de définir des commandes personnalisées qui pourront être appelées sous la forme
            `brew ma_commande --option1 <recette>.`
  Ces commandes personnalisées sont des scripts Shell exécutables (ou des scripts Ruby), qui doivent être mis dans le $PATH, et avoir comme nom de fichier brew-ma_commande


## CREER SES PROPRES FORMULAE

Les recettes sont des scripts Ruby.

Pour en savoir plus allez voir: https://zestedesavoir.com/tutoriels/357/homebrew-pour-gerer-ses-logiciels-os-x/#6-11118_ecrire-sa-propre-recette
